/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package projekahir;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Aji Andriawan
 */
public class ProjekAhir {

    public static void main(String[] args) {
        //inisialisasi
        int pin, pilihan, tarik, tambah, proses;
        int saldo = 50000;
        //input
        Scanner input = new Scanner(System.in);
        //proses
        try {
            System.out.println("|------------------------------------------|");
            System.out.println("|                                          |");
            System.out.println("|                    ATM                   |");
            System.out.println("|              BANK  PALCOMTECH            |");
            System.out.println("|               SELAMAT DATANG             |");
            System.out.println("|                                          |");
            System.out.println("|------------------------------------------|");
            System.out.println("|SILAHKAN MASUKAN PIN ANDA :               |");
            System.out.print("|->> ");
            pin = input.nextInt();
            if (pin == 112233) {

                do {
                    System.out.println("|------------------------------------------|");
                    System.out.println("|                REKENING ANDA             |");
                    System.out.println("|            A/n : AJI ANGRI AWAN          |");
                    System.out.println("|------------------------------------------|");
                    System.out.println("|               MENU PELAYANAN             |");
                    System.out.println("|<- 1.LIHAT SALDO          3. TARIK SALDO->|");
                    System.out.println("|<- 2.TAMBAH SALDO         4. KELUAR     ->|");
                    System.out.println("|------------------------------------------|");
                    System.out.println("|SILAHKAN PILIH MENU PELAYANAN             |");
                    System.out.print("|->> ");
                    pilihan = input.nextInt();
                    System.out.println("|------------------------------------------|");

                    switch (pilihan) { // Perulangan Case
                        case 1:
                            System.out.println("============================================");
                            System.out.println("|SALDO ANDA SAAT INI :Rp. " + saldo + "|");
                            System.out.println("============================================");
                            break;

                        case 2:

                            System.out.println("============================================");
                            System.out.println("               Tambah Saldo                 ");
                            System.out.println("============================================");
                            System.out.println("|------------------------------------------|");
                            System.out.println("|Masukan jumlah uang                       |");
                            System.out.print("|->>Rp. ");
                            tambah = input.nextInt();
                            System.out.println("|------------------------------------------|");
                            System.out.println("============================================");
                            System.out.println("|Saldo Awal        :Rp. " + saldo + "|");
                            System.out.println("|Penambahan        :Rp. " + tambah + "|");
                            saldo = saldo + tambah;
                            System.out.println("|Saldo akhir       :Rp. " + saldo + "|");
                            System.out.println("============================================");

                            System.out.println("|------------------------------------------|");
                            System.out.println("|SILAHKAN MASUKAN PIN ANDA                 |");
                            System.out.print("|->> ");
                            pin = input.nextInt();
                            System.out.println("|------------------------------------------|");

                            if (pin == 112233) {
                                System.out.println("============================================");
                                System.out.println("|-> PIN MATCH                              |");
                                System.out.println("============================================");
                            } else {
                                saldo = saldo - tambah;
                                System.out.println("============================================");
                                System.out.println("|-> PIN NOT MATCH. Trasaksi Gagal          |");
                                System.out.println("============================================");
                                break;
                            }

                            if (tambah < 1000) {
                                saldo = saldo - tambah;
                                System.out.println("============================================");
                                System.out.println("|->MAAF MINIMAL TAMBAH SALDO RP.1000       |");
                                System.out.println("|->TRANSAKSI GAGAL                         |");
                                System.out.println("============================================");
                            } else {
                                System.out.println("============================================");
                                System.out.println("|TRANSAKSI BERHASIL                        |");
                                System.out.println("============================================");
                            }
                            System.out.println("|------------------------------------------|");
                            break;

                        case 3:

                            System.out.println("============================================");
                            System.out.println("                 TARIK SALDO                ");
                            System.out.println("============================================");
                            System.out.println("|------------------------------------------|");
                            System.out.println("|Masukan jumlah uang                       |");
                            System.out.print("|->>Rp. ");
                            tarik = input.nextInt();
                            System.out.println("|------------------------------------------|");
                            System.out.println("============================================");
                            System.out.println("|Saldo Awal        :Rp. " + saldo + "|");
                            System.out.println("|Penarikan         :Rp. " + tarik + "|");
                            saldo = saldo - tarik;
                            System.out.println("|Saldo akhir       :Rp. " + saldo + "|");
                            System.out.println("============================================");

                            System.out.println("|------------------------------------------|");
                            System.out.println("|SILAHKAN MASUKAN PIN ANDA ");
                            System.out.print("|->> ");
                            pin = input.nextInt();
                            System.out.println("|------------------------------------------|");

                            if (pin == 112233) {
                                System.out.println("============================================");
                                System.out.println("|-> PIN MATCH                              |");
                                System.out.println("============================================");
                            } else {
                                saldo = saldo + tarik;
                                System.out.println("============================================");
                                System.out.println("|-> PIN NOT MATCH. Trasaksi Gagal          |");
                                System.out.println("============================================");
                                break;
                            }


                            
                            if (saldo < 50000){
                                saldo = saldo + tarik;
                                System.out.println("============================================");
                                System.out.println("|-> SALDO TIDAK CUKUP : TRANSAKSI GAGAL !  |");
                                System.out.println("============================================");

                            } else {
                                System.out.println("============================================");
                                System.out.println("|TRANSAKSI BERHASIL                        |");
                                System.out.println("============================================");
                            }
                            System.out.println("|------------------------------------------|");
                            break;
                            
                        case 4 :
                            System.out.println("============================================");
                            System.out.println("|              TERIMA KASIH TELAH          |");
                            System.out.println("|         MENGGUNAKAN BANK PALCOMTECH      |");
                            System.out.println("============================================");
                            break;
            

                        default:
                            System.out.println("============================================");
                            System.out.println("|         Maaf Menu Yang Anda Tekan        |");
                            System.out.println("|         Tidak Tersedia Di Pilihan        |");
                            System.out.println("============================================");

                    }
                    System.out.println("|------------------------------------------|");
                    System.out.println("|              TRANSAKSI LAGI?             |");
                    System.out.println("|<-TEKAN 0 UNTUK YA | TEKAN 1 UNTUK TIDAK->|");
                    System.out.print("|->> ");
                    proses = input.nextInt();
                    ++proses;
                    System.out.println("|------------------------------------------|");
                    System.out.println("============================================");
                    System.out.println("============================================");

                } while (proses <= 1);
            } else {
                System.out.println("============================================");
                System.out.println("|    PIN ANDA SALAH ! SILAKAN COBA LAGI    |");
                System.out.println("|    ATAU HUBUNGI KANTOR CABANG TERDEKAT   |");
                System.out.println("============================================");
            }

        } catch (InputMismatchException ime) {
            System.out.println("============================================");
            System.out.println("|            PIN ANDA HARUS ANGKA          |");
            System.out.println("============================================");
        } finally {
            System.out.println("============================================");
            System.out.println("|              TERIMA KASIH TELAH          |");
            System.out.println("|         MENGGUNAKAN BANK PALCOMTECH      |");
            System.out.println("============================================");
        }
    }
}
